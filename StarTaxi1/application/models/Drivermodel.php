<?php
class Drivermodel extends CI_Model{

    function ride_rental_later_cancel($query,$booking_id)
    {
        $this->db->where('rental_booking_id',$booking_id)
                ->update('rental_booking',$query);
        $this->db->where(['booking_id'=>$booking_id,'ride_mode'=>2])
            ->update('table_user_rides',['driver_id'=>0]);
    }

    function ride_later_cancel($query,$booking_id)
    {
        $this->db->where('ride_id',$booking_id)
                ->update('ride_table',$query);
        $this->db->where(['booking_id'=>$booking_id,'ride_mode'=>1])
            ->update('table_user_rides',['driver_id'=>0]);
    }

    function ride_rental_later_Accept($query,$booking_id)
    {
        $this->db->where('rental_booking_id',$booking_id)
                 ->update('rental_booking',$query);
        $driver_id = $query['driver_id'];
        $this->db->where(['booking_id'=>$booking_id,'ride_mode'=>2])
            ->update('table_user_rides',['driver_id'=>$driver_id]);
    }

    function ride_later_Accept($query,$booking_id)
    {
        $this->db->where('ride_id',$booking_id)
                 ->update('ride_table',$query);
        $driver_id = $query['driver_id'];
        $this->db->where(['booking_id'=>$booking_id,'ride_mode'=>1])
                 ->update('table_user_rides',['driver_id'=>$driver_id]);
    }

    function new_ride_later($driver_id,$city_id)
    {
        $date = date("Y-m-d");
        $data = $this->db->select('*')
                         ->from('table_user_rides')
                         ->order_by("ride_date", "ASC")
                         ->where(['driver_id'=>0,'city_id'=>$city_id,'ride_date >='=>$date,'ride_type'=>2])
                         ->get();
        return $data->result_array();
    }

    function online_time($driver_id)
    {
        $date = date('Y-m-d');
        $query = $this->db->select('*')
                         ->from('table_driver_online')
                         ->where(['driver_id'=>$driver_id,'online_date'=>$date])
                         ->get();
        return $query->row();
    }

    function avarage_rating($driver_id)
    {
        $query = $this->db->select('AVG(user_rating_star) as average_score')
                          ->from('table_normal_ride_rating')
                          ->where(['driver_id'=>$driver_id])
                          ->get();
        return $query->row()->average_score;
    }

    function all_rides($driver_id)
    {
        $date = date('Y-m-d');
        $data = $this->db->from('ride_allocated')
                         ->where(['allocated_driver_id'=>$driver_id,'allocated_date'=>$date])
                         ->get();
       return $data->num_rows();
    }

    function all_acceptance_rides($driver_id)
    {
        $date = date('Y-m-d');
        $data = $this->db->from('ride_allocated')
                         ->where(['allocated_driver_id'=>$driver_id,'allocated_date'=>$date,'allocated_ride_status'=>1])
                         ->get();
        return $data->num_rows();
    }

    function add_new_car($data)
    {
        $this->db->insert('table_driver_cars',$data);
    }

    function edit_profile($driver_id,$data)
    {
        $this->db->where(['driver_id'=>$driver_id])
                  ->update('table_driver',$data);
    }

    function change_password($driver_id,$new_password,$old_password)
    {
        $this->db->where(['driver_id'=>$driver_id,'driver_password'=>$old_password])
            ->update('table_driver',['driver_password'=>$new_password]);
        return $this->db->affected_rows();
    }

    function check_bankdetails($driver_id)
    {
        $data = $this->db->get_where('table_driver',['driver_id'=>$driver_id]);
        return $data->row();
    }

    function driver_rides($driver_id)
    {
        $data = $this->db->select('*')
            ->from('table_user_rides')
            ->order_by("user_ride_id", "desc")
            ->where(['driver_id'=>$driver_id])
            ->get();
        return $data->result_array();
    }

    function check_phone($phone)
    {
        $data = $this->db->get_where('table_driver',['driver_phone'=>$phone]);
        return $data->row();
    }

    function driver_profile($driver_id,$driver_token)
    {
        $data = $this->db->get_where('driver',['driver_id'=>$driver_id,'driver_token'=>$driver_token]);
        return $data->row();
    }

    public function add_device($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function car_type($car_type_id)
    {
        $data = $this->db->get_where('table_car_type',['car_type_id'=>$car_type_id]);
        return $data->row();
    }

    function car_model($car_model_id)
    {
        $data = $this->db->get_where('table_car_model',['car_model_id'=>$car_model_id]);
        return $data->row();
    }

    function driver_online($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function driver_offline($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

}