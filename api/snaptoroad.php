<?php
function Get_distance($admin_panel_map_key,$ride_id,$start,$finish,$path)
{
    $lat_long_from_application = $path;
    $path_array = explode("|",$path);
    $count_path_array = count($path_array);
    if($count_path_array > 100){
        $average = ceil($count_path_array/100);
        $new_array = array();
        for ($i = 0; $i < $count_path_array; $i=$i+$average) {
            $lat_long = $path_array[$i];
            $new_array[] =$lat_long;
        }
        $path = implode("|",$new_array);
    }
    $path = $start."|".$path."|".$finish;
    $geocode=file_get_contents('https://roads.googleapis.com/v1/snapToRoads?path='.$path.'&interpolate=false&key='.$admin_panel_map_key);
    $output = json_decode($geocode);
    $combine = array();
    foreach($output->snappedPoints as $login){
        $latitude = $login->location->latitude;
        $longitude = $login->location->longitude;
        $combine[] = $latitude.",".$longitude;
        $sanp_to_rod = implode("|",$combine);
    }
    $start = array_shift($combine);
    $finish = array_pop($combine);
    $count_combine = count($combine);
    if($count_combine > 23){
        $average_way = ceil($count_combine/22);
        $new_array1 = array();
        for ($j = 0; $j < $count_combine; $j=$j+$average_way) {
            $lat_long = $combine[$j];
            $new_array1[] = $lat_long;
        }
        $waypoints = implode("|",$new_array1);
    }else{
        $waypoints = implode("|",$combine);
    }
    $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&destination='.$finish.'&waypoints='.$waypoints.'&key='.$admin_panel_map_key;
    $data = file_get_contents($url);
    $output= json_decode($data);
    $distance = $output->routes[0]->legs;
    $a = array();
    foreach($distance as $location){
        $dist = $location->distance->value;
        $a[] = $dist;
    }
    $re = array_sum($a);
    $log  = "snap to road  Api - : ".date("F j, Y, g:i a").PHP_EOL.
        "ride_id :".$ride_id.PHP_EOL.
        "start: ".$start.PHP_EOL.
        "finish: ".$finish.PHP_EOL.
        "lat_long_from_application :".$lat_long_from_application.PHP_EOL.
        "waypoints: ".$waypoints.PHP_EOL.
        "Snaptoroad api input: ".$path.PHP_EOL.
        "Snaptoroad api output: ".$sanp_to_rod.PHP_EOL.
        "distnce: ".$re.PHP_EOL.
        "-------------------------".PHP_EOL;
    file_put_contents('../logfile/snaptoroad_'.date("j.n.Y").'.txt', $log, FILE_APPEND);

  return $re;
}
function Get_google_distance($admin_panel_map_key,$start,$stop)
{
    $details_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $start . "&destinations=" . $stop . "&mode=driving&key=".$admin_panel_map_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $details_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = json_decode(curl_exec($ch));
    $dis = $view_data['distance'][1] = $response->rows[0]->elements[0]->distance->value;
    return $dis;
}
?>