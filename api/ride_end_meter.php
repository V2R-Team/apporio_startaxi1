<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';
include 'location_fromlatlog.php';
include 'ride-sms.php';
include 'snaptoroad.php';
$ride_id = $_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$end_lat=$_REQUEST['end_lat'];
$end_long=$_REQUEST['end_long'];
$meter_distance = $_REQUEST['distance'];
$driver_token=$_REQUEST['driver_token'];
//$language_id=$_REQUEST['language_id'];
$lat_long = $_REQUEST['lat_long'];
$language_id=1;

if($ride_id!="" && $driver_id!="" && $end_lat!="" && $end_long!="" && $driver_token!= "" )
{
    $query4="select * from ride_table where ride_id='$ride_id'";
    $result4 = $db->query($query4);
    $list4=$result4->row;
    $ride_status = $list4['ride_status'];
    if ($ride_status == 6)
    {
        $query="select * from driver where driver_token='$driver_token' AND driver_id='$driver_id'";
        $result = $db->query($query);
        $ex_rows=$result->num_rows;
        $list = $result->row;
        $total_payment_eraned = $list['total_payment_eraned'];
        $company_payment = $list['company_payment'];
        $driver_payment = $list['driver_payment'];
        $commision = $list['commission'];
        if($ex_rows==1)
        {
            $last_time_stamp = date("h:i:s A");
            $end_time = date("h:i:s A");
            $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
            $data=$dt->format('M j');
            $day=date("l");
            $date=$day.", ".$data ;
            $new_time=date("h:i");
            $completed_rides = $list['completed_rides']+1;
            $city_id = $list['city_id'];
            $car_type_id = $list['car_type_id'];

            $query5="UPDATE driver SET last_update='$new_time',completed_rides='$completed_rides',busy=0 WHERE driver_id='$driver_id'" ;
            $db->query($query5);

            $query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='7' WHERE ride_id='$ride_id'" ;
            $db->query($query1);



            $query2 = "select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
            $result2 = $db->query($query2);
            $list3 = $result2->row;
            $distance_unit = $list3['distance_unit'];
            $base_distance = $list3['base_distance'];
            $base_distance_price = $list3['base_distance_price'];
            $base_price_per_unit = $list3['base_price_per_unit'];


            $free_ride_minutes = $list3['free_ride_minutes'];
            $price_per_ride_minute = $list3['price_per_ride_minute'];

            $free_waiting_time = $list3['base_wating_time'];
            $wating_price_minute = $list3['wating_price_minute'];
            $query3="select * from done_ride where ride_id='$ride_id'";
            $result3 = $db->query($query3);
            $list=$result3->row;
            $begin_lat = $list['begin_lat'];
            $begin_long = $list['begin_long'];
            $start = $begin_lat.",".$begin_long;
            $finish = $end_lat.",".$end_long;
            $query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
            $result = $db->query($query);
            $admin_settings = $result->row;
            $admin_panel_map_key = $admin_settings['admin_panel_map_key'];
            if ($lat_long != "")
            {
                $distance = Get_distance($admin_panel_map_key,$ride_id,$start,$finish,$lat_long);
            }else{
                $distance = Get_google_distance($admin_panel_map_key,$start,$finish);
            }

            if($distance_unit == "Km"){
                $dist1 = ($distance/1000);
            }else{
                $dist1 = $distance*0.00062137;
            }
            $dist1 = sprintf("%.2f",$dist1);
            if($dist1 <= $base_distance)
            {
                $final_amount = $base_distance_price;
                $final_amount = sprintf("%.2f",$final_amount);
            }
            else
            {
                $diff_distance = $dist1-$base_distance;
                $amount1= ($diff_distance * $base_price_per_unit);
                $final_amount = $base_distance_price+$amount1;
                $final_amount = sprintf("%.2f",$final_amount);
            }

            $done_ride=$list['done_ride_id'];
            $begin_time = $list['begin_time'];
            $waiting_time = $list['waiting_time'];

            $datetime1 = strtotime($begin_time);
            $datetime2 = strtotime($end_time);
            $interval  = abs($datetime2 - $datetime1);
            $ride_time   = round($interval / 60);

            $dist = $dist1." ".$distance_unit;
            $end_location = getAddress($admin_panel_map_key,$end_lat,$end_long);
            $end_location = $end_location?$end_location:'Address Not found';
            $query2="UPDATE done_ride SET meter_distance='$meter_distance',end_lat='$end_lat',end_long='$end_long', end_location='$end_location', end_time='$end_time', amount='$final_amount', distance='$dist',tot_time='$ride_time' WHERE ride_id='$ride_id'";
            $db->query($query2);


            if($ride_time > $free_ride_minutes)
            {
                $diff_distance = $ride_time-$free_ride_minutes;
                $amount1= ($diff_distance * $price_per_ride_minute);
                $amount1 = sprintf("%.2f", $amount1);
                $query2="UPDATE done_ride SET  ride_time_price='$amount1' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }else{
                $query2="UPDATE done_ride SET  ride_time_price='00.00' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }


            if($waiting_time > $free_waiting_time)
            {
                $diff_distance = $waiting_time-$free_waiting_time;
                $final_amount1= ($diff_distance * $wating_price_minute);
                $final_amount1 = sprintf("%.2f", $final_amount1);
                $query2="UPDATE done_ride SET waiting_price='$final_amount1' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }else{
                $query2="UPDATE done_ride SET waiting_price='00.00' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }
            $user_id = $list4['user_id'];
            $pem_file = $list4['pem_file'];
            $coupan = $list4['coupon_code'];
            $ride_time = $list4['ride_time'];
            $card_id=$list4['card_id'];
            $payment_option_id=$list4['payment_option_id'];
            $ride_time = $list4['ride_time'];
            $extra_charges_day = date("l");

            $query="select * from done_ride where ride_id='$ride_id'";
            $result = $db->query($query);
            $list12345 = $result->row;
            $waiting_price = $list12345['waiting_price'];
            $amount =  $list12345['amount'];
            $ride_time_price =  $list12345['ride_time_price'];
            $night_time_charge = $list12345['night_time_charge'];
            $peak_time_charge = $list12345['peak_time_charge'];
            $total_amount =  $waiting_price+$amount+$ride_time_price+$night_time_charge+$peak_time_charge;

            $query2 = "select * from extra_charges where city_id='$city_id' and extra_charges_day='$extra_charges_day'";
            $result2 = $db->query($query2);
            $extra_charges = $result2->row;
            if(!empty($extra_charges)){
                $slot_one_starttime = $extra_charges["slot_one_starttime"];
                $slot_one_endtime = $extra_charges["slot_one_endtime"];
                $slot_two_starttime = $extra_charges["slot_two_starttime"];
                $slot_two_endtime = $extra_charges["slot_two_endtime"];
                $payment_type = $extra_charges["payment_type"];
                $slot_price = $extra_charges["slot_price"];
                $time = date("h:i A",strtotime($ride_time));
                $ride_strtotime = strtotime($time);
                $slot_one_starttime = strtotime($slot_one_starttime);
                $slot_one_endtime = strtotime($slot_one_endtime);
                $slot_two_starttime = strtotime($slot_two_starttime);
                $slot_two_endtime = strtotime($slot_two_endtime);
                if($ride_strtotime >= $slot_one_starttime && $slot_one_endtime >= $ride_strtotime || $ride_strtotime >= $slot_two_starttime && $slot_one_endtime >= $slot_two_endtime)
                {
                    if($payment_type == 1){
                        $peak_time_charge = $slot_price;
                    }else{
                        $peak_time_charge = $total_amount*$slot_price;
                    }
                }
                else
                {
                    $peak_time_charge = "0.00";
                }
                $query2="UPDATE done_ride SET  peak_time_charge='$peak_time_charge' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }
            $query2 = "select * from extra_charges where city_id='$city_id' and extra_charges_type=2";
            $result2 = $db->query($query2);
            $extra_charges = $result2->row;
            if(!empty($extra_charges)){
                $slot_one_starttime = $extra_charges["slot_one_starttime"];
                $slot_one_endtime = $extra_charges["slot_one_endtime"];
                $payment_type = $extra_charges["payment_type"];
                $slot_price = $extra_charges["slot_price"];
                $time = date("h:i A",strtotime($ride_time));
                $ride_strtotime = strtotime($time);
                $slot_one_starttime = strtotime($slot_one_starttime);
                $slot_one_endtime = strtotime($slot_one_endtime);
                if($ride_strtotime >= $slot_one_starttime && $slot_one_endtime >= $ride_strtotime)
                {
                    if($payment_type == 1){
                        $night_time_charge = $slot_price;
                        $total_amount = $total_amount+$night_time_charge;
                    }else{
                        $night_time_charge = $total_amount*$slot_price;
                    }
                }
                else
                {
                    $night_time_charge = "0.00";
                }
                $query2="UPDATE done_ride SET  night_time_charge='$night_time_charge' WHERE ride_id='$ride_id'";
                $db->query($query2);
            }


            $query="select * from done_ride where ride_id='$ride_id'";
            $result = $db->query($query);
            $list12345 = $result->row;
            $waiting_price = $list12345['waiting_price'];
            $amount =  $list12345['amount'];
            $ride_time_price =  $list12345['ride_time_price'];
            $night_time_charge = $list12345['night_time_charge'];
            $peak_time_charge = $list12345['peak_time_charge'];
            $total_amount =  $waiting_price+$amount+$ride_time_price+$night_time_charge+$peak_time_charge;

            if($coupan != "")
            {
                $query1 ="select * from coupons where coupons_code='$coupan'";
                $result1 = $db->query($query1);
                $list1 = $result1->row;
                $coupon_type = $list1['coupon_type'];
                $coupon_code = $list1['coupons_code'];
                $coupan_price = $list1['coupons_price'];
                if($coupon_type == "Nominal"){
                    if($coupan_price > $total_amount){
                        $total_amount = "0.00";
                    }else{
                        $total_amount = $total_amount-$coupan_price;
                    }

                }else{
                    $coupan_price = ($total_amount*$coupan_price)/100;
                    $total_amount =  $total_amount-$coupan_price;
                }
            }else{
                $coupan_price  = "0.00";
            }
            switch ($payment_option_id) {
                case "1" :
                    $total_payed_amount = $total_amount;
                    $query="select * from payment_option where payment_option_id='$payment_option_id'";
                    $result = $db->query($query);
                    $payment =$result->row;
                    $payment_method = $payment['payment_option_name'];
                    $payment_platform = "Admin";
                    $url = 'http://apporio.org/StarTaxi1/api/payment_saved.php';
                    $ch = curl_init ();
                    $fields = array ('order_id'=>$done_ride,'user_id'=>$user_id,'payment_id'=>1,'payment_method'=>$payment_method,'payment_platform'=>$payment_platform,'payment_amount'=>$total_amount,'payment_date_time'=>$date,'payment_status'=>"Done");
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt ( $ch, CURLOPT_POSTFIELDS,$fields);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_exec($ch);
                    curl_close($ch);
                    break;
                case "3":
                    $admin_panel_stripe_key = $admin_settings['admin_panel_stripe_key'];
                    require_once('Stripe/lib/Stripe.php');
                    Stripe::setApiKey($admin_panel_stripe_key);
                    Stripe::$apiBase = "https://api.stripe.com";
                    $total_payed_amount = $total_amount;
                    $query="select * from card where card_id='$card_id'";
                    $result = $db->query($query);
                    $list=$result->row;
                    $customer_id=$list['customer_id'];
                    try
                    {
                        $charge = Stripe_Charge::create(array( "amount" => $total_amount*100,"currency" => "usd","customer" => $customer_id,));
                        $payment_id = $charge['id'];
                        $status= $charge['outcome']['seller_message'];
                        $query="select * from payment_option where payment_option_id='$payment_option_id'";
                        $result = $db->query($query);
                        $list=$result->row;
                        $payment_method = $list['payment_option_name'];
                        $payment_platform = "Stripe";
                        $url = 'http://apporio.org/StarTaxi1/api/payment_saved.php';
                        $ch = curl_init ();
                        $fields = array ('order_id'=>$done_ride,'user_id'=>$user_id,'payment_id'=>$payment_option_id,'payment_method'=>$payment_method,'payment_platform'=>$payment_platform,'payment_amount'=>$total_amount,'payment_date_time'=>$date,'payment_status'=>"Done");
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt ( $ch, CURLOPT_POSTFIELDS,$fields);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_exec($ch);
                        curl_close($ch);
                    }catch(Stripe_CardError $e)
                    {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    }
                    catch (Stripe_InvalidRequestError $e) {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    } catch (Stripe_AuthenticationError $e) {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    } catch (Stripe_ApiConnectionError $e) {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    } catch (Stripe_Error $e) {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    } catch (Exception $e) {
                        $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                    }
                    break;
                case "4":
                    $query="select * from user where user_id =$user_id";
                    $result = $db->query($query);
                    $list2344=$result->row;
                    $wallet_money = $list2344['wallet_money'];
                    if($wallet_money < $total_amount){
                        $total_payed_amount = $wallet_money;
                        $remain = "0.00";
                        $remain1 = $total_amount-$wallet_money;
                        $payment_falied_message = "Insufficent Balance In Wallet";
                        $query3="UPDATE done_ride SET wallet_deducted_amount='$wallet_money',total_payable_amount='$remain1', payment_falied_message='$payment_falied_message' WHERE ride_id='$ride_id'";
                        $db->query($query3);
                        $url = 'http://apporio.org/StarTaxi1/api/payment_saved.php';
                        $ch = curl_init ();
                        $fields = array ('admin'=>1,'order_id'=>$done_ride,'user_id'=>$user_id,'payment_id'=>$payment_option_id,'payment_method'=>"Wallet",'payment_platform'=>"Admin",'payment_amount'=>$wallet_money,'payment_date_time'=>$date,'payment_status'=>"Done");
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt ( $ch, CURLOPT_POSTFIELDS,$fields);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_exec($ch);
                        curl_close($ch);
                    }else{
                        $total_payed_amount = $total_amount;
                        $remain = $wallet_money-$total_amount;
                        $query="select * from payment_option where payment_option_id='$payment_option_id'";
                        $result = $db->query($query);
                        $list=$result->row;
                        $payment_method = $list['payment_option_name'];
                        $payment_platform = "Admin";
                        $url = 'http://apporio.org/StarTaxi1/api/payment_saved.php';
                        $ch = curl_init ();
                        $fields = array ('order_id'=>$done_ride,'user_id'=>$user_id,'payment_id'=>$payment_option_id,'payment_method'=>$payment_method,'payment_platform'=>$payment_platform,'payment_amount'=>$total_amount,'payment_date_time'=>$date,'payment_status'=>"Done");
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt ( $ch, CURLOPT_POSTFIELDS,$fields);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_exec($ch);
                        curl_close($ch);
                    }
                    $sql2="UPDATE user SET wallet_money='$remain' WHERE user_id='$user_id'";
                    $db->query($sql2);
                    break;
                case "2":
                    $payment_falied_message = "You Have Select Paypal";
                    $query3="UPDATE done_ride SET total_payable_amount='$total_amount', payment_falied_message='$payment_falied_message' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                    break;
            }

            if($commision != 0){
                $commision_price = ($total_amount*$commision)/100;
                $company_commision = number_format((float)$commision_price, 2, '.', '');
                $driver_amount = $total_amount-$company_commision;
                $driver_amount=number_format((float)$driver_amount, 2, '.', '');
            }else{
                $company_commision = "0.00";
                $driver_amount = $total_amount;
            }


            $query2="UPDATE done_ride SET  total_amount='$total_amount',company_commision='$company_commision',driver_amount='$driver_amount',coupan_price='$coupan_price' WHERE ride_id='$ride_id'";
            $db->query($query2);

            $query5="select * from user_device where user_id='$user_id' AND login_logout=1";
            $result5 = $db->query($query5);
            $list5=$result5->rows;
            $language="select * from messages where language_id='$language_id' and message_id=28";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;
            $message=$lang_list['message_name'];
            $done_ride= (String) $done_ride;
            $ride_status= (String) $ride_status;

            if (!empty($list5))
            {
                foreach ($list5 as $user)
                {
                    $device_id = $user['device_id'];
                    $flag = $user['flag'];
                    if($flag == 1)
                    {
                        IphonePushNotificationCustomer($device_id,$message,$done_ride,$ride_status,$pem_file);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id,$message,$done_ride,$ride_status);
                    }
                }
            }else{
                $query5="select * from user where user_id='$user_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;
                $device_id=$list5['device_id'];
                if($device_id!="")
                {
                    if($list5['flag'] == 1)
                    {
                        IphonePushNotificationCustomer($device_id, $message,$done_ride,$ride_status,$pem_file);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id, $message,$done_ride,$ride_status);
                    }
                }
            }



            $query4="select * from user where user_id='$user_id'";
            $result4 = $db->query($query4);
            $list41=$result4->rows;
            $user_phone = $list41['user_phone'];
            //ride_end_sms($user_phone);
            $query3="select * from done_ride where ride_id='$ride_id'";
            $result3 = $db->query($query3);
            $list=$result3->row;
            $list['payment_option_id']=$payment_option_id;
            $re = array('result'=> 1,'msg'=> $message,'details'	=> $list);

        }
        else {
            $re = array('result'=> 419,'msg'=> "No Record Found",);
        }
    }else{
        $re = array('result'=> 0,'msg'=> "Ride Status Already Changed");
    }
}
else
{
    $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
$log  = "ride end Api - : ".date("F j, Y, g:i a").PHP_EOL.
    "ride_id: ".$ride_id.PHP_EOL.
    "driver_id: ".$driver_id.PHP_EOL.
    "end_lat: ".$end_lat.PHP_EOL.
    "end_long: ".$end_long.PHP_EOL.
    "meter_distance: ".$meter_distance.PHP_EOL.
    "lat_long: ".$lat_long.PHP_EOL.
    "response: ".json_encode($re).PHP_EOL.
    "-------------------------".PHP_EOL;
file_put_contents('../logfile/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
echo json_encode($re, JSON_PRETTY_PRINT);
?>