<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query1="select * from application_version";
$result1 = $db->query($query1);
$list=$result1->row;
if(isset($_POST['save']))
{
    $query1="UPDATE application_version SET ios_current_version='".$_POST['ios_current_version']."',ios_mandantory_update='".$_POST['ios_mandantory_update']."',android_current_version='".$_POST['android_current_version']."',android_mandantory_update='".$_POST['android_mandantory_update']."' WHERE application_version_id=1";
    $db->query($query1);
    $db->redirect("home.php?pages=app-version");
}
?>
<script type="text/javascript">
    function Validate() {
        var ios_current_version = document.getElementById("ios_current_version").value;
        var android_current_version = document.getElementById("android_current_version").value;
        if (ios_current_version == "")
        {
            alert("Enter IOS Application Version.");
            return false;
        }
        if (android_current_version == "")
        {
            alert("Enter Android Application Version.");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Manage Application Update</h3>
        <span>
    </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" onsubmit="return Validate()">


                            <div class="form-group ">
                                <label class="control-label col-lg-2">IOS Current Version</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Current Version" name="ios_current_version" id="ios_current_version" value="<?php echo $list['ios_current_version'];?>">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2"></label>
                                <div class="col-lg-10">
                                    <label><input type="checkbox" name="ios_mandantory_update" value="1" <?php echo ($list['ios_mandantory_update'] ==1 ? 'checked' : '');?>>   Mandantory Update</label>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Android Current Version</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Android Current Version" name="android_current_version" id="android_current_version" value="<?php echo $list['android_current_version'];?>">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2"></label>
                                <div class="col-lg-10">
                                    <label><input type="checkbox" name="android_mandantory_update" value="1" <?php echo ($list['android_mandantory_update']==1 ? 'checked' : '');?>>   Mandantory Update</label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12"  id="save" name="save" value="Submit" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
</body>
</html>
