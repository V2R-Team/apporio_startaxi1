<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$driver_id = $_GET['id'];
$query ="select * from table_driver_bill WHERE driver_id='$driver_id' ORDER BY bill_id DESC";
$result = $db->query($query);
$list = $result->rows;

if(!empty($list)){
    require_once 'PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Billed Date');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Bill Period');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Bill Amount');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Status');
    $row = 2;
    foreach($list as $value)
    {
        $bill_to_date = $value['bill_to_date'];
        $bill_to_date = date("d/m/Y", strtotime($bill_to_date));
        $bill_from_date = date("d/m/Y", strtotime($value['bill_from_date']));
        $bill_to_date = date("d/m/Y", strtotime($value['bill_to_date']));
        $end = $bill_from_date." To ".$bill_to_date;
        $bill_status = $value['bill_status'];
        if($bill_status == 1)
        {
            $s =  "Settled";
        }else{
            $s =  "Unsettled";
        }
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $bill_to_date);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $end);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['outstanding_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $s);
        $row++;
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=driver-bills.xlsx");
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}else{
    echo '<script type="text/javascript">alert("No Data For Export")</script>';
    $db->redirect("home.php?pages=accounts");
}
?>