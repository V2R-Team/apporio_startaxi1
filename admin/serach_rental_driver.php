<?php
include_once '../apporioconfig/start_up.php';
$car_type = $_REQUEST['car_type'];
$orig_latitude = $_REQUEST['orig_latitude'];
$orig_longitude = $_REQUEST['orig_longitude'];
$data="select * from driver WHERE car_type_id='$car_type' AND online_offline=1 and driver_admin_status=1 and busy=0 and login_logout=1";
$result = $db->query($data);
$list=$result->rows;
$c = array();
foreach($list as $login3)
{
    $driver_lat = $login3['current_lat'];
    $driver_long =$login3['current_long'];

    $theta = $orig_longitude - $driver_long;
    $dist = sin(deg2rad($orig_latitude)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($orig_latitude)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $km=$miles* 1.609344;
    $km = sprintf("%.2f",$km);
    if($km <= 10)
    {
        $c[] = array("driver_id"=> $login3['driver_id'],"driver_name"=>$login3['driver_name'],"distance" => $km,);
    }
}
foreach ($c as $value)
{
    $distance[] = $value['distance'];
}
array_multisort($distance,SORT_ASC,$c);
echo "<option value=''>Select Driver</option>";
foreach($c as $data_value)
{
    $driver_name = $data_value['driver_name']."(".$data_value['distance']." Km)";
    echo "<option value='".$data_value['driver_id']."'>".$driver_name."</option>";
}
?>
